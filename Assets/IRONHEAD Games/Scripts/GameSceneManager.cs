﻿using System.Collections;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class GameSceneManager : MonoBehaviour
{
    [Header("UI")] public TextMeshProUGUI timeText;
    public Image progressBarImage;
    public GameObject timerUI_Gameobject;

    [Header("Managers")] public GameObject cubeSpawnManager;

    //Audio related
    float audioClipLength;
    private float timeToStartGame = 5.0f;
    public float gameTime = -1f;

    public GameObject currentScoreUiGameObject;
    public GameObject finalScoreUiGameObject;

    public GameObject swordGameObject;
    public GameObject gunGameObject;

    public GameObject leftControllerGameObject;
    public GameObject rightControllerGameObject;
    
    [SerializeField] private GameObject UIHelper;


    // Start is called before the first frame update
    void Start()
    {
        //Getting the duration of the song
        audioClipLength = AudioManager.instance.musicTheme.clip.length;
        Debug.Log(audioClipLength);

        if (Mathf.Approximately(gameTime, -1f))
        {
            StartCoroutine(StartCountdown(audioClipLength));
        }
        else
        {
            StartCoroutine(StartCountdown(gameTime));
        }
        //Starting the countdown with song

        //Resetting progress bar
        progressBarImage.fillAmount = Mathf.Clamp(0, 0, 1);

        finalScoreUiGameObject.SetActive(false);
        currentScoreUiGameObject.SetActive(true);

        swordGameObject.SetActive(true);
        gunGameObject.SetActive(true);

        leftControllerGameObject.SetActive(false);
        UIHelper.SetActive(false);
        rightControllerGameObject.SetActive(false);
    }


    public IEnumerator StartCountdown(float countdownValue)
    {
        while (countdownValue > 0)
        {
            yield return new WaitForSeconds(1.0f);
            countdownValue -= 1;

            timeText.text = ConvertToMinAndSeconds(countdownValue);

            progressBarImage.fillAmount = (AudioManager.instance.musicTheme.time / audioClipLength);
        }

        GameOver();
    }


    public void GameOver()
    {
        Debug.Log("Game Over");
        timeText.text = ConvertToMinAndSeconds(0);

        //Disable cube spawning
        cubeSpawnManager.SetActive(false);

        //Disable timer UI
        timerUI_Gameobject.SetActive(false);

        finalScoreUiGameObject.SetActive(true);
        currentScoreUiGameObject.SetActive(false);

        finalScoreUiGameObject.transform.rotation = Quaternion.Euler(Vector3.zero);
        finalScoreUiGameObject.transform.position =
            GameObject.FindWithTag("Player").transform.position + new Vector3(0, 2.0f, 4.0f);

        swordGameObject.SetActive(false);
        gunGameObject.SetActive(false);

        leftControllerGameObject.SetActive(true);
        rightControllerGameObject.SetActive(true);
        UIHelper.SetActive(true);

    }

    public void LoadLobby(string sceneName)
    {
        SceneLoader.instance.LoadScene(sceneName);
    }
    
    private string ConvertToMinAndSeconds(float totalTimeInSeconds)
    {
        string timeText = Mathf.Floor(totalTimeInSeconds / 60).ToString("00") + ":" +
                          Mathf.FloorToInt(totalTimeInSeconds % 60).ToString("00");
        return timeText;
    }
}