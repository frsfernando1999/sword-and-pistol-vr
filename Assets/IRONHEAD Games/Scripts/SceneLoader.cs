using System.Collections;
using UnityEngine;

public class SceneLoader : MonoBehaviour
{
    public static SceneLoader instance;

    [SerializeField] private OVROverlay overlayBackground;
    [SerializeField] private OVROverlay overlayLoadingText;

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(gameObject);
    }

    public void LoadScene(string sceneName)
    {
        StartCoroutine(ShowOverlayAndLoad(sceneName));
    }

    private IEnumerator ShowOverlayAndLoad(string sceneName)
    {
        overlayBackground.enabled = true;
        overlayLoadingText.enabled = true;

        GameObject centerEyeAnchor = GameObject.Find("CenterEyeAnchor");
        overlayLoadingText.gameObject.transform.position = centerEyeAnchor.transform.position + new Vector3(0f, 0f, 3f);
        
        yield return new WaitForSeconds(3f);

        AsyncOperation asyncLoad = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(sceneName);
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
        
        overlayBackground.enabled = false;
        overlayLoadingText.enabled = false;

        yield return null;
    }
}
